<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
    exit;
} else if ($_SESSION['usuarioTipo'] == 'c') {
    header("Location: view/indexClientes.php");
} else if ($_SESSION['usuarioTipo'] == 'e') {
    header("Location: view/indexEmpresa.php");
}
?>
<html>
    <head>
        <title>Teste EAD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="js/FuncoesCliente.js.js"></script>
        <script type="text/javascript" src="js/FuncoesServicos.js"></script>
        <script type="text/css" src="css/styleEAD.css"></script>
        <>
    </head>   
</html>

