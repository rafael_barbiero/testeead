6<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
    exit;
}
?>

<html>
    <head>
        <title>Teste EAD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="../js/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="../js/FuncoesServicos.js"></script>        
    </head>
    <body onload="ConsultarServicos();" >
        <form>
            <div align="center">
                <table>
                    <tr>
                        <td><label>Serviço</label></td>
                        <td><input id="nome" name="nome" ></td>
                    </tr>                
                    <tr>
                        <td></td>                        
                        <td>
                            <input type="button" onclick="CadastrarServicos();" value="Cadastrar">
                            <input type="button" onclick="ConsultarServicos();" value="Consultar">
                        </td>
                    </tr>                      
                </table>                
            </div>            
            <div align="center">                                
                <table id="tableServicos" ></table>                
            </div>
            <div align="center"> 
                <a href="../index.php">Inicio</a>
                <a href="../controller/logout.php">Sair</a>
            </div>
        </form>
    </body>
</html>
