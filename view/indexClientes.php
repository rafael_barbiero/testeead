<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
    exit;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script type="text/css" src="../css/styleEAD.css"></script>
    </head>
    <body>
        <div align="center">
            <table>
                <tr>                    
                    <td><a href="ClienteServicos.php">Serviços</a></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div align="center">
            <a href="../index.php">Inicio</a>
            <a href="../controller/logout.php">Sair</a>
        </div>
    </body>
</html>
