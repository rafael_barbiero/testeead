<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
session_start();
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
    exit;
}
?>

<html>
    <head>
        <title>Teste EAD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="../js/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="../js/FuncoesCliente.js"></script>
        <script type="text/css" src="../css/styleEAD.css"></script>
    </head>
    <body onload="ConsultarCliente();" >
        <form>
            <div align="center">
                <table>
                    <tr>
                        <td><label>Nome</label></td>
                        <td><input id="nome" name="nome" ></td>                        
                    </tr>
                    <tr>
                        <td><label>Login</label></td>
                        <td><input id="login" name="login" ></td>                        
                    </tr>
                    <tr>
                        <td><label>Senha</label></td>
                        <td><input id="senha" name="senha" ></td>                        
                    </tr>
                    <tr>
                        <td></td>                        
                        <td>
                            <input type="button" onclick="CadastrarCliente();" value="Cadastrar">
                            <input type="button" onclick="ConsultarCliente();" value="Consultar">
                        </td>
                    </tr>                      
                </table>                
            </div> 
            <div align="center">
                <a href="../index.php">Inicio</a>
                <a href="../controller/logout.php">Sair</a>
            </div>
            <div align="center">                                
                <table id="tableClientes" ></table>                
            </div>
        </form>
    </body>
</html>
