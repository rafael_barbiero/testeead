<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

include_once("model/Cliente.php");
include_once("dao/ClienteDao.php");
?>
<html>
    <head>
        <title>Teste EAD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="js/Funcoes.js"></script>
    </head>
    <body>
        <form action="controller/logon.php">
            <div align="center">
                <table>
                    <tr>
                        <td><label>Nome</label></td>
                        <td><input id="login" name="login" value="adm"></td>
                    </tr>                                                        
                    <tr>
                        <td><label>Senha</label></td>
                        <td><input id="senha" name="senha" value="adm"></td>
                    </tr>         
                    <tr align="center">                        
                        <td colspan="2">
                            <input type="submit" value="Logar">
                        </td>
                    </tr> 
                </table>                
            </div> 
           
            <div align="center">                                
                <table id="tableClientes" ></table>                
            </div>
        </form>
    </body>
</html>
