<?php

/**
 * Description of ClienteController
 *
 * @author rafael
 */
include_once("ClienteInterface.php");

class ClienteDao implements ClienteInterface {

    public function inserir($cliente = Cliente) {

        try {
            $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');

            $statementUsuario = $connecton->prepare('INSERT INTO USUARIO ( ID, LOGIN, SENHA, TIPO) VALUES(?, ?, ?, ?)');

            $statementUsuario->bindValue(1, null, PDO::PARAM_STR);
            $statementUsuario->bindValue(2, $cliente->getLogin(), PDO::PARAM_STR);
            $statementUsuario->bindValue(3, $cliente->getSenha(), PDO::PARAM_STR);
            $statementUsuario->bindValue(4, 'c', PDO::PARAM_STR);

            if ($statementUsuario->execute()) {
                
                $statement = $connecton->prepare('INSERT INTO CLIENTE (ID, NOME) VALUES(?, ?)');
                
                $statement->bindValue(1, null, PDO::PARAM_STR);
                $statement->bindValue(2, $cliente->getNome(), PDO::PARAM_STR);

                if ($statement->execute()) {
                    return "Usuario Inserido com sucesso";
                } else {
                    return "Erro ao inserir";
                }
            }
        } catch (Exception $exc) {
            return "Erro ao inserir: " . $exc;
        }
    }

    public function editar($cliente = Cliente) {

        $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');


        $statement = $connecton->prepare('UPDATE CLIENTE SET NOME = ? WHERE ID = ?');

        $statement->bindValue(1, $cliente->getNome(), PDO::PARAM_STR);
        $statement->bindValue(2, $cliente->getId(), PDO::PARAM_INT);


        try {

            if ($statement->execute()) {
                return "Usuario editado com sucesso";
            } else {
                return "Erro ao editar";
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        return $retorno;
    }

    public function deletar($cliente = Cliente) {

        $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');

        $statement = $connecton->prepare('DELETE FROM CLIENTE WHERE ID = ?');
        $statement->bindValue(1, $cliente->getId(), PDO::PARAM_INT);

        try {

            if ($statement->execute()) {
                return "Usuario deletado com sucesso";
            } else {
                return "Erro ao deletar";
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        return $retorno;
    }

    public function consultar($cliente = Cliente) {

        $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');

        $nome = $cliente->getNome();

        try {

            $sql = "SELECT NOME, ID FROM CLIENTE";

            if ($nome != "") {
                $sql = $sql . " WHERE NOME = :nome";
                $statement = $connecton->prepare($sql);
                $statement->bindParam(':nome', $nome, PDO::PARAM_STR);
            } else {
                $statement = $connecton->prepare($sql);
            }

            $statement->execute();
            $clientes = $statement->fetchAll(PDO::FETCH_ASSOC);

            if ($statement->rowCount() > 0) {
                foreach ($clientes as $cliente_listar) {
                    $clienteArray[] = $cliente_listar;
                }
                return $clienteArray;
            } else {
                return array(array("NOME" => "Não há registros para busca!", "ID" => 'X'));
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
