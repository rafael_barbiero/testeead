<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
interface ClienteInterface {

    public function inserir($cliente = Cliente);

    public function editar($cliente = Cliente);

    public function deletar($cliente = Cliente);

    public function consultar($cliente = Cliente);
}
