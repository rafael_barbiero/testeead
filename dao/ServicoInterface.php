<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
interface ServicoInterface {

    public function inserir($servico = Servico);

    public function editar($servico = Servico);

    public function deletar($servico = Servico);

    public function consultar($servico = Servico);
}
