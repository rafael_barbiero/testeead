<?php
/**
 * Description of ClienteController
 *
 * @author rafael
 */

include_once("ServicoInterface.php");

class ServicoDao implements ServicoInterface {

    public function inserir($servico = Servico) {

        $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');


        $statement = $connecton->prepare('INSERT INTO SERVICOS (ID, NOME) VALUES(?, ?)');

        $statement->bindValue(1, null, PDO::PARAM_STR);
        $statement->bindValue(2, $servico->getNome(), PDO::PARAM_STR);


        try {
            if ($statement->execute()) {
                return "Usuario Inserido com sucesso";
            } else {
                return "Erro ao inserir";
            }
        } catch (Exception $exc) {
            return "Erro ao inserir: " . $exc;
        }
    }

    public function editar($servico = Servico) {

        $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');


        $statement = $connecton->prepare('UPDATE SERVICOS SET NOME = ? WHERE ID = ?');

        $statement->bindValue(1, $servico->getNome(), PDO::PARAM_STR);
        $statement->bindValue(2, $servico->getId(), PDO::PARAM_INT);


         try {

            if ($statement->execute()) {
                return "Usuario editado com sucesso";
            } else {
                return "Erro ao editar";
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        return $retorno;
    }

    public function deletar($servico = Servico) {

        $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');

        $statement = $connecton->prepare('DELETE FROM SERVICOS WHERE ID = ?');
        $statement->bindValue(1, $servico->getId(), PDO::PARAM_INT);
        
        try {

            if ($statement->execute()) {
                return "Usuario deletado com sucesso";
            } else {
                return "Erro ao deletar";
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        return $retorno;
    }

    public function consultar($servico = Servico) {

        $connecton = new PDO('mysql:host=localhost; dbname=Teste', 'TesteEAD', 'TesteEAD');

        $nome = $servico->getNome();

        try {

            $sql = "SELECT NOME, ID FROM SERVICOS";

            if ($nome != "") {
                $sql = $sql . " WHERE NOME = :nome";
                $statement = $connecton->prepare($sql);
                $statement->bindParam(':nome', $nome, PDO::PARAM_STR);
            } else {
                $statement = $connecton->prepare($sql);
            }

            $statement->execute();            

            if ($statement->rowCount() > 0) {
                foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $servico_listar) {
                    $servicoArray[] = $servico_listar;
                }
                return $servicoArray;
            } else {
                return array(array("NOME" => "Não há registros para busca!", "ID" => 'X'));
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
