/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function CadastrarServicos() {
    var nome = document.getElementById('nome').value;
    $.ajax({
        url: '../controller/servico/inserirServico.php',
        data: "nome=" + nome,
        success: function (data) {
            alert(data);
            document.getElementById('nome').value = '';            
        }
    }).done(function (){window.location.reload();});
}
function ConsultarServicos() {
   
    var nome = document.getElementById('nome').value;
    $('#tableServicos').children().remove();

    $.ajax({
        url: '../controller/servico/consultarServico.php',
        data: "nome=" + nome,
        success: function (data) {
             var array = data.split('-');
                array.forEach(function (valor, chave) {
                    if (valor !== "") {
                        valorId = valor.split(":");
                        var linha = $('<tr  name=linhas><td>Nome:</td><td><label id='+ valorId[1] +'>'+ valorId[0] + '</label/></td>' +                                    
                                    '<td></td><td><input type=button value=Excluir onclick=DeletarServico(' + valorId[1] + '); ></td>'+
                                    '<td></td><td><a href="javascript: AbrePopup('+ valorId[1] +', \''+ valorId[0] +'\');">Editar</td>'+
                                    '</tr>');
                        $('#tableServicos').append(linha);
                    }
                });
        }       
    });
}

function ConsultarClienteServicos() {
   
    var nome = document.getElementById('nome').value;
    $('#tableServicos').children().remove();

    $.ajax({
        url: '../controller/servico/consultarServico.php',
        data: "nome=" + nome,
        success: function (data) {
             var array = data.split('-');
                array.forEach(function (valor, chave) {
                    if (valor !== "") {
                        valorId = valor.split(":");
                        var linha = $('<tr  name=linhas><td>Nome:</td><td><label id='+ valorId[1] +'>'+ valorId[0] + '</label/></td>' +                                    
                                    '<td></td><td><input type=button value=Contratar onclick=ContratarServico(' + valorId[1] + '); ></td>'+                                    
                                    '</tr>');
                        $('#tableServicos').append(linha);
                    }
                });
        }       
    });
}

function ContratarServico(){
    
}

function EditarServico(id, nome) {        
        
    $.ajax({
        url: '../controller/servico/editarServico.php',
        data: "id=" + id +"&nome=" + nome,
        success: function (data) {            
            alert(data);         
            document.getElementById('nome').value = '';            
        }
    }).done(function (){
        window.opener.location.reload();
        window.close();
    });
    
}

function DeletarServico(id) {    
    $.ajax({
        url: '../controller/servico/deletarServico.php',
        data: "id=" + id,
        success: function (data) {
            alert(data);          
        }        
    }).done(function (){
        window.location.reload();        
    });
}

function AbrePopup(id, nome) {    
    
    var param = 'id='+ id +'&nome='+ nome;    
    window.open('PopUpEditarServico.php?' + param, '_blank', 'fullscreen=no,height=100,width=500');    
}


