/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function CadastrarCliente() {
    
    var nome = document.getElementById('nome').value;
    var login = document.getElementById('login').value;
    var senha = document.getElementById('senha').value;
    
    $.ajax({
        url: '../controller/cliente/inserirCliente.php',
        data: "nome=" + nome +'&login=' + login + '&senha=' + senha,
        success: function (data) {
            alert(data);
            document.getElementById('nome').value = '';
        },
        error: function (data) {
            alert('Erro ao cadastrar cliente' + data);
        }
    }).done(function () {
        window.location.reload();
    });
}
function ConsultarCliente() {
    var nome = document.getElementById('nome').value;
    $('#tableClientes').children().remove();

    $.ajax({
        url: '../controller/cliente/consultarCliente.php',
        data: "nome=" + nome,
        success: function (data) {

            var array = data.split('-');
            array.forEach(function (valor, chave) {
                if (valor !== "") {
                    valorId = valor.split(":");
                    var linha = $('<tr  name=linhas><td>Nome:</td><td><label id=' + valorId[1] + '>' + valorId[0] + '</label/></td>' +
                            '<td></td><td><input type=button value=Excluir onclick=DeletarCliente(' + valorId[1] + '); ></td>' +
                            '<td></td><td><a href="javascript: AbrePopup(' + valorId[1] + ', \'' + valorId[0] + '\');">Editar</td>' +
                            '</tr>');
                    $('#tableClientes').append(linha);
                }
            });
        },
        error: function (data) {
            alert('Erro ao consultar cliente');
        }
    });
}
function EditarCliente(id, nome) {
    $.ajax({
        url: '../controller/cliente/editarCliente.php',
        data: "id=" + id + "&nome=" + nome,
        success: function (data) {
            alert(data);
            document.getElementById('nome').value = '';
        },
        error: function (data) {
            alert('Erro ao editar cliente');
        }
    }).done(function () {
        window.opener.location.reload();
        window.close();

    });

}

function DeletarCliente(id) {
    $.ajax({
        url: '../controller/cliente/deletarCliente.php',
        data: "id=" + id,
        success: function (data) {
            alert(data);
        },
        error: function (data) {
            alert('Erro ao deletar cliente');
        }
    }).done(function () {
        window.location.reload();
        window.close();
    });
}


function AbrePopup(id, nome) {
    var param = 'id=' + id + '&nome=' + nome;
    window.open('PopUpEditarCliente.php?' + param, '_blank', 'fullscreen=no,height=100,width=500');
}


